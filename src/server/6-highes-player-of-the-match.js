const csvParser = require("csv-parser");
const fs = require("fs");
const matchesCsvFilePath =
  "/home/user/javaSript/js-ipl-data-project/src/data/matches.csv";
let matches = [];

fs.createReadStream(matchesCsvFilePath)
  .pipe(csvParser())
  .on("data", (match) => {
    return matches.push(match);
  })
  .on("end", () => {
    try {
      const directoryPath =
        "/home/user/javaSript/js-ipl-data-project/src/public/output/"; //home/user/javaSript/js-ipl-data-project/src/public
      const ResultFilePath =
        "/home/user/javaSript/js-ipl-data-project/src/public/output/6-highes-player-of-the-match.json";
      fs.mkdirSync(directoryPath, { recursive: true });
      writeDataToJsonFile(
        ResultFilePath,
        JSON.stringify(highest_player_of_the_match(matches))
      );
    } catch (error) {
      console.error("Error:", error.message);
    }
  });

const writeDataToJsonFile = (jsonWriteFilePath, resultData) => {
  fs.writeFile(jsonWriteFilePath, resultData, (error) => {
    if (error) {
      console.log("Error occur during writing data to json file", error);
    } else {
      console.log("Result written to the file successfully");
    }
  });
};

/* function highest_player_of_the_match(jsonFile) {
  let res = jsonFile.reduce((acc, curr) => {
    if (!acc[curr.season]) {
      acc[curr.season] = {};
    }
    if (!acc[curr.season][curr.player_of_match]) {
      acc[curr.season][curr.player_of_match] = 1;
    } else {
      acc[curr.season][curr.player_of_match] += 1;
    }
    return acc;
  }, {});


  let ans = Object.keys(res).reduce((acc, curr) => {
    let max = 0;
    Object.keys(res[curr]).forEach((each) => {
      max = max < res[curr][each] ? res[curr][each] : max;
    });
    Object.keys(res[curr]).forEach((each) => {
      if (res[curr][each] == max) {
        acc[curr] = {};
        acc[curr][each] = max;
      }
    });
    return acc;
  }, {});
  return ans
} */

/* ------------------------------- */

function highest_player_of_the_match(jsonFile)  {
  let result = {};
  for (let each of jsonFile) {
    if (!result[each.season]) {
      result[each.season] = {};
    }
    if (!result[each.season][each.player_of_match]) {
      result[each.season][each.player_of_match] = 1;
    } else {
      result[each.season][each.player_of_match] += 1;
    }
  }
  
  let final = {};
  for (let each in result) {
    let max = 0;
    for (key in result[each]) {
      max = max < result[each][key] ? result[each][key] : max;
    }
    for (key in result[each]) {
      if (result[each][key] == max) {
        final[each] = {};
        final[each][key] = max;
      }
    }
  }
  return final
}
