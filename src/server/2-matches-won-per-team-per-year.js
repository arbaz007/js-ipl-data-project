const csvParser = require("csv-parser");
const fs = require("fs");
const matchesCsvFilePath =
  "/home/user/javaSript/js-ipl-data-project/src/data/matches.csv";

let matches = [];
//Parsing the csv file and return result as array of objects.
fs.createReadStream(matchesCsvFilePath)
  .pipe(csvParser())
  .on("data", (match) => {
    return matches.push(match);
  })
  .on("end", () => {
    try {
      const directoryPath =
        "/home/user/javaSript/js-ipl-data-project/src/public/output/"; //home/user/javaSript/js-ipl-data-project/src/public
      const ResultFilePath =
        "/home/user/javaSript/js-ipl-data-project/src/public/output/2-matches-won-per-team-per-year.json";
      fs.mkdirSync(directoryPath, { recursive: true });
      writeDataToJsonFile(
        ResultFilePath,
        JSON.stringify(matches_won_per_team_per_year(matches))
      );
    } catch (error) {
      console.error("Error:", error.message);
    }
  });

const writeDataToJsonFile = (jsonWriteFilePath, resultData) => {
  fs.writeFile(jsonWriteFilePath, resultData, (error) => {
    if (error) {
      console.log("Error occur during writing data to json file", error);
    } else {
      console.log("Result written to the file successfully");
    }
  });
};
//Number of matches won per team per year in IPL.

/* function matches_won_per_team_per_year(jsonFile) {
  let res = jsonFile.reduce((acc, curr) => {
    acc[curr.winner] = !acc[curr.winner] ? {} : acc[curr.winner];
    acc[curr.winner][curr.season] = !acc[curr.winner][curr.season]
      ? 1
      : acc[curr.winner][curr.season] + 1;
    return acc;
  }, {});
  return res;
} */

/* ---------------------------------------------------------- */

function matches_won_per_team_per_year(jsonFile) {
  let result = {};
  for (let each of jsonFile) {
    result[each.winner] = !result[each.winner] ? {} : result[each.winner];
    result[each.winner][each.season] = !result[each.winner][each.season]
      ? 1
      : result[each.winner][each.season] + 1;
  }

  return(result);
}
