const csvParser = require("csv-parser");
const fs = require("fs");
const matchesCsvFilePath =
  "/home/user/javaSript/js-ipl-data-project/src/data/deliveries.csv";
const CsvFilePath =
  "/home/user/javaSript/js-ipl-data-project/src/data/matches.csv";
let matches = [];
let jsonArray = []

fs.createReadStream(CsvFilePath).pipe(csvParser()).on("data", (data) => {
  return jsonArray.push(data)
})

fs.createReadStream(matchesCsvFilePath)
  .pipe(csvParser())
  .on("data", (match) => {
    return matches.push(match);
  })
  .on("end", () => {
    try {
      const directoryPath =
        "/home/user/javaSript/js-ipl-data-project/src/public/output/"; //home/user/javaSript/js-ipl-data-project/src/public
      const ResultFilePath =
        "/home/user/javaSript/js-ipl-data-project/src/public/output/7-strike-rate-of-batsman.json";
      fs.mkdirSync(directoryPath, { recursive: true });
      writeDataToJsonFile(
        ResultFilePath,
        JSON.stringify(strike_rate_of_batsman(matches))
      );
    } catch (error) {
      console.error("Error:", error.message);
    }
  });

const writeDataToJsonFile = (jsonWriteFilePath, resultData) => {
  fs.writeFile(jsonWriteFilePath, resultData, (error) => {
    if (error) {
      console.log("Error occur during writing data to json file", error);
    } else {
      console.log("Result written to the file successfully");
    }
  });
};

function getSeason(id) {
  let ans;
  jsonArray.forEach((each) => {
    if (each.id == id) {
      ans = each.season;
    }
  });
  return ans;
}

/* function strike_rate_of_batsman(deliveries) {
  let runs = deliveries.reduce((acc, curr) => {
    let season = getSeason(curr.match_id);
  
    if (!acc[curr.batsman]) {
      acc[curr.batsman] = {};
    }
    if (!acc[curr.batsman][season]) {
      acc[curr.batsman][season] = { runs: 0, balls: 0 };
    }
    acc[curr.batsman][season]["runs"] += +curr.batsman_runs;
    if (curr.noball_runs == "0" || curr.wide_runs == "0") {
      acc[curr.batsman][season]["balls"] += 1;
    }
    return acc;
  }, {});
  
  let strike = Object.keys(runs).reduce((acc, curr) => {
      
    Object.keys(runs[curr]).forEach((each) => {
      if (!acc[curr]) {
        acc[curr] = {};
      }
      let strike_rate = (runs[curr][each]["runs"] / runs[curr][each]["balls"]) * 100
      acc[curr][each] = strike_rate
    });
    return acc
  }, {});
  
  return strike;
  
}
 */







/* -------------------------------------------------------------- */




function strike_rate_of_batsman(deliveries)  {
  let final = {}
  let result = {}
  
  for (let each of deliveries) {
    let season = getSeason(each.match_id)
    if (!result[each.batsman]) {
      result[each.batsman] = {};
    }
    if (!result[each.batsman][season]) {
      result[each.batsman][season] = { runs: 0, balls: 0 };
    }
    result[each.batsman][season]["runs"] += +each.batsman_runs;
    if (each.noball_runs == "0" || each.wide_runs == "0") {
      result[each.batsman][season]["balls"] += 1;
    }
  }
  for (let key in result) {
    for (let each in result[key]) {
      if (!final[key]) {
        final[key] = {};
      }
      let strike_rate = (runs[key][each]["runs"] / runs[key][each]["balls"]) * 100
      final[key][each] = strike_rate
    }
  }
  
  return final
}