const csvParser = require("csv-parser");
const fs = require("fs");
const matchesCsvFilePath =
  "/home/user/javaSript/js-ipl-data-project/src/data/deliveries.csv";
let matches = [];

fs.createReadStream(matchesCsvFilePath)
  .pipe(csvParser())
  .on("data", (match) => {
    return matches.push(match);
  })
  .on("end", () => {
    try {
      const directoryPath =
        "/home/user/javaSript/js-ipl-data-project/src/public/output/"; //home/user/javaSript/js-ipl-data-project/src/public
      const ResultFilePath =
        "/home/user/javaSript/js-ipl-data-project/src/public/output/8-one-player-dismiiss-by-another.json";
      fs.mkdirSync(directoryPath, { recursive: true });
      writeDataToJsonFile(
        ResultFilePath,
        JSON.stringify(one_player_dismissed_by_another(matches))
      );
    } catch (error) {
      console.error("Error:", error.message);
    }
  });

const writeDataToJsonFile = (jsonWriteFilePath, resultData) => {
  fs.writeFile(jsonWriteFilePath, resultData, (error) => {
    if (error) {
      console.log("Error occur during writing data to json file", error);
    } else {
      console.log("Result written to the file successfully");
    }
  });
};

/* function one_player_dismissed_by_another(deliveries) {
  let dismiss = deliveries.reduce((acc, curr) => {
    if (curr.player_dismissed != "") {
      if (!acc[curr.player_dismissed]) {
        acc[curr.player_dismissed] = {};
      }
      if (!acc[curr.player_dismissed][curr.bowler]) {
        acc[curr.player_dismissed][curr.bowler] = 1;
      } else {
        acc[curr.player_dismissed][curr.bowler] += 1;
      }
    }
    return acc;
  }, {});
  let ans = Object.keys(dismiss).reduce((acc, curr) => {
    let max = 0;
    Object.keys(dismiss[curr]).forEach((each) => {
      max = dismiss[curr][each] > max ? dismiss[curr][each] : max;
    });
    Object.keys(dismiss[curr]).forEach((each) => {
      if (dismiss[curr][each] == max) {
        acc[curr] = each;
      }
    });
    return acc;
  }, {});
  return ans;
} */

/* -------------------------------------------------------------------- */

function one_player_dismissed_by_another(deliveries) {
  let result = {};
  for (let each of deliveries) {
    if (each.player_dismissed != "") {
      if (!result[each.player_dismissed]) {
        result[each.player_dismissed] = {};
      }
      if (!result[each.player_dismissed][each.bowler]) {
        result[each.player_dismissed][each.bowler] = 1;
      } else {
        result[each.player_dismissed][each.bowler] += 1;
      }
    }
  }

  let final = {};
  for (let key in result) {
    let max = 0;
    for (let each in result[key]) {
      max = result[key][each] > max ? result[key][each] : max;
    }
    for (let each in result[key]) {
      if (result[key][each] == max) {
        final[key] = each;
      }
    }
  }

  return final
}
