const csvParser = require("csv-parser");
const fs = require("fs");
const matchesCsvFilePath =
  "/home/user/javaSript/js-ipl-data-project/src/data/deliveries.csv";
let matches = [];
let jsonArray = []

fs.createReadStream(CsvFilePath).pipe(csvParser()).on("data", (data) => {
  return jsonArray.push(data)
})

fs.createReadStream(matchesCsvFilePath)
  .pipe(csvParser())
  .on("data", (match) => {
    return matches.push(match);
  })
  .on("end", () => {
    try {
      const directoryPath =
        "/home/user/javaSript/js-ipl-data-project/src/public/output/"; //home/user/javaSript/js-ipl-data-project/src/public
      const ResultFilePath =
        "/home/user/javaSript/js-ipl-data-project/src/public/output/4-top-10-economical-bowler.json";
      fs.mkdirSync(directoryPath, { recursive: true });
      writeDataToJsonFile(
        ResultFilePath,
        JSON.stringify(top_10_economical_bowler(matches))
      );
    } catch (error) {
      console.error("Error:", error.message);
    }
  });

const writeDataToJsonFile = (jsonWriteFilePath, resultData) => {
  fs.writeFile(jsonWriteFilePath, resultData, (error) => {
    if (error) {
      console.log("Error occur during writing data to json file", error);
    } else {
      console.log("Result written to the file successfully");
    }
  });
};

function getMatchId(season) {
  let res = jsonArray.filter((each) => {
    if (each.season == season) {
      return each;
    }
  });
  return [res[0].id, res[res.length - 1].id];
}

/* function top_10_economical_bowler(deliveries) {
  let [match_id_1, match_id_2] = getMatchId(2015);
  let ans = deliveries.reduce((acc, curr) => {
    if (curr.match_id >= +match_id_1 && curr.match_id <= +match_id_2) {
      if (!acc[curr.bowler]) {
        acc[curr.bowler] = { balls: 0, runs: 0 };
      }
      if (acc[curr.bowler]) {
        acc[curr.bowler]["runs"] += +curr.total_runs;
        if (curr.ball <= 6) {
          acc[curr.bowler]["balls"] += 1;
        }
      }
    }
    return acc;
  }, {});

  let res = Object.keys(ans).reduce((acc, curr) => {
    let obj = {};
    obj["name"] = curr;
    obj["avg"] = ans[curr]["runs"] / ans[curr]["balls"];
    acc.push(obj);
    return acc;
  }, []);

  res.sort((bowler1, bowler2) => {
    return bowler1.avg - bowler2.avg;
  });

  let top10 = []
  for (let index = 0; index < 10; index++) {
    top10.push(res[index].name)
  }
  return top10;
} */

/* ------------------------------------------------------------------- */

function top_10_economical_bowler(deliveries) {
  let result = {};
  
  for (let each of deliveries) {
    let [match_id_1, match_id_2] = getMatchId(2015);
    if (each.match_id >= +match_id_1 && each.match_id <= +match_id_2) {
      if (!result[each.bowler]) {
        result[each.bowler] = { balls: 0, runs: 0 };
      }
      if (result[each.bowler]) {
        result[each.bowler]["runs"] += +each.total_runs;
        if (each.ball <= 6) {
          result[each.bowler]["balls"] += 1;
        }
      }
    }
  }
  
  let final = [];
  
  for (let key in result) {
    let obj = {};
    obj["name"] = key;
    obj["avg"] = ans[key]["runs"] / ans[key]["balls"];
    final.push(obj);
  }
  final.sort((bowler1, bowler2) => {
    return bowler1.avg - bowler2.avg;
  });
  let top10 = []
  for (let index = 0; index < 10; index++) {
    top10.push(final[index].name)
  }
  return top10
  
}