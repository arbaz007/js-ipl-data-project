const csvParser = require("csv-parser");
const fs = require("fs");
const matchesCsvFilePath =
  "/home/user/javaSript/js-ipl-data-project/src/data/deliveries.csv";
let matches = [];

fs.createReadStream(matchesCsvFilePath)
  .pipe(csvParser())
  .on("data", (match) => {
    return matches.push(match);
  })
  .on("end", () => {
    try {
      const directoryPath =
        "/home/user/javaSript/js-ipl-data-project/src/public/output/"; //home/user/javaSript/js-ipl-data-project/src/public
      const ResultFilePath =
        "/home/user/javaSript/js-ipl-data-project/src/public/output/9-best-economy-in-superover.json";
      fs.mkdirSync(directoryPath, { recursive: true });
      writeDataToJsonFile(
        ResultFilePath,
        JSON.stringify(best_economy_in_super_over(matches))
      );
    } catch (error) {
      console.error("Error:", error.message);
    }
  });

const writeDataToJsonFile = (jsonWriteFilePath, resultData) => {
  fs.writeFile(jsonWriteFilePath, resultData, (error) => {
    if (error) {
      console.log("Error occur during writing data to json file", error);
    } else {
      console.log("Result written to the file successfully");
    }
  });
};

/* function best_economy_in_super_over(deliveries) {
  let res = deliveries.reduce((acc, curr) => {
    if (curr.is_super_over != "0") {
      if (!acc[curr.bowler]) {
        acc[curr.bowler] = { runs: 0, balls: 0 };
      }
      if (curr.bye_runs != "0" || curr.legbye_runs != "0") {
        acc[curr.bowler]["runs"] += 0;
        acc[curr.bowler]["balls"] += 1;
      }
      if (curr.noball_runs != "0" || curr.wide_runs != "0") {
        acc[curr.bowler]["runs"] += +curr.total_runs;
        acc[curr.bowler]["balls"] += 0;
      } else {
        acc[curr.bowler]["runs"] += +curr.total_runs;
        acc[curr.bowler]["balls"] += 1;
      }
    }
    return acc;
  }, {});

  let ans = [];
  Object.keys(res).forEach((each) => {
    let obj = {};
    obj["name"] = each;
    let avg = res[each]["runs"] / res[each]["balls"];
    obj["avg"] = avg;
    ans.push(obj);
  });
  ans.sort((pl1, pl2) => {
    return pl1.avg - pl2.avg;
  });
  return ans[0].name;
} */


/* ----------------------------------------------------------------- */


function best_economy_in_super_over(deliveries) {
  let result = {};
  for (let each of deliveries) {
    if (each.is_super_over != "0") {
      if (!result[each.bowler]) {
        result[each.bowler] = { runs: 0, balls: 0 };
      }
      if (each.bye_runs != "0" || each.legbye_runs != "0") {
        result[each.bowler]["runs"] += 0;
        result[each.bowler]["balls"] += 1;
      }
      if (each.noball_runs != "0" || each.wide_runs != "0") {
        result[each.bowler]["runs"] += +each.total_runs;
        result[each.bowler]["balls"] += 0;
      } else {
        result[each.bowler]["runs"] += +each.total_runs;
        result[each.bowler]["balls"] += 1;
      }
    }
  }
  
  let final = [];
  for (let key in result) {
    let obj = {};
    obj["name"] = key;
    let avg = result[key]["runs"] / result[key]["balls"];
    obj["avg"] = avg;
    final.push(obj);
  }
  
  return final[0].name
}
