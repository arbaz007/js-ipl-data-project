const csvParser = require("csv-parser");
const fs = require("fs");
const matchesCsvFilePath =
  "/home/user/javaSript/js-ipl-data-project/src/data/matches.csv";
let matches = [];

fs.createReadStream(matchesCsvFilePath)
  .pipe(csvParser())
  .on("data", (match) => {
    return matches.push(match);
  })
  .on("end", () => {
    try {
      const directoryPath =
        "/home/user/javaSript/js-ipl-data-project/src/public/output/"; //home/user/javaSript/js-ipl-data-project/src/public
      const ResultFilePath =
        "/home/user/javaSript/js-ipl-data-project/src/public/output/5-team-won-toss-match.json";
      fs.mkdirSync(directoryPath, { recursive: true });
      writeDataToJsonFile(
        ResultFilePath,
        JSON.stringify(team_won_toss_match(matches))
      );
    } catch (error) {
      console.error("Error:", error.message);
    }
  });

const writeDataToJsonFile = (jsonWriteFilePath, resultData) => {
  fs.writeFile(jsonWriteFilePath, resultData, (error) => {
    if (error) {
      console.log("Error occur during writing data to json file", error);
    } else {
      console.log("Result written to the file successfully");
    }
  });
};

/* 
function team_won_toss_match(jsonFile) {
    let res = jsonFile.reduce((acc, curr) => {
        if (!acc[curr.winner]) {
            if (curr.winner == curr.toss_winner) {
                acc[curr.winner] = 1
            }
        }else {
            if (curr.winner == curr.toss_winner) {
                acc[curr.winner] += 1
            }
        }
        return acc
    }, {})
    
    return res;
}
 */


/* --------------------------------------------------------- */


function team_won_toss_match(jsonFile) {
    let result = {}
    
    for (let each of jsonFile) {
        if (!result[each.winner]) {
            if (each.winner == each.toss_winner) {
                result[each.winner] = 1
            }
        }else {
            if (each.winner == each.toss_winner) {
                result[each.winner] += 1
            }
        }
    }
    return result
}