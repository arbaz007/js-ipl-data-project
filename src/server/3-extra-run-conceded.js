const csvParser = require("csv-parser");
const fs = require("fs");
const matchesCsvFilePath =
  "/home/user/javaSript/js-ipl-data-project/src/data/deliveries.csv";
let matches = [];
let jsonArray = []

fs.createReadStream(CsvFilePath).pipe(csvParser()).on("data", (data) => {
  return jsonArray.push(data)
})


fs.createReadStream(matchesCsvFilePath)
  .pipe(csvParser())
  .on("data", (match) => {
    return matches.push(match);
  })
  .on("end", () => {
    try {
      const directoryPath =
        "/home/user/javaSript/js-ipl-data-project/src/public/output/"; //home/user/javaSript/js-ipl-data-project/src/public
      const ResultFilePath =
        "/home/user/javaSript/js-ipl-data-project/src/public/output/3-extra-run-conceded.json";
      fs.mkdirSync(directoryPath, { recursive: true });
      writeDataToJsonFile(
        ResultFilePath,
        JSON.stringify(extra_runs_conceed(matches))
      );
    } catch (error) {
      console.error("Error:", error.message);
    }
  });

const writeDataToJsonFile = (jsonWriteFilePath, resultData) => {
  fs.writeFile(jsonWriteFilePath, resultData, (error) => {
    if (error) {
      console.log("Error occur during writing data to json file", error);
    } else {
      console.log("Result written to the file successfully");
    }
  });
};


function getMatchId(season) {
  let res = jsonArray.filter((each) => {
    if (each.season == season) {
      return each;
    }
  });
  return [res[0].id, res[res.length - 1].id];
}


/* function extra_runs_conceed(deliveries) {
  let [match_id_1, match_id_2] = getMatchId(2016)
  let extra = deliveries.reduce((acc, curr) => {
    if (curr.match_id >= +match_id_1 && curr.match_id <= +match_id_2) {
      acc[curr.bowling_team] = !acc[curr.bowling_team]
        ? +curr.extra_runs
        : acc[curr.bowling_team] + +curr.extra_runs;
    }
    return acc;
  }, {});
  return extra
} */


  /* ----------------------------------------------------------- */



function extra_runs_conceed(deliveries) {
  let results = {};
  for (let each of deliveries) {
    let [match_id_1, match_id_2] = getMatchId(2016);
    if (each.match_id >= +match_id_1 && each.match_id <= +match_id_2) {
      results[each.bowling_team] = !results[each.bowling_team]
        ? +each.extra_runs
        : results[each.bowling_team] + +each.extra_runs;
    }
  }
  
  return results; 
}